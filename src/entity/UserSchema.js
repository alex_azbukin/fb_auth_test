const EntitySchema = require("typeorm").EntitySchema; // import {EntitySchema} from "typeorm";
const User = require("../model/User").User; // import {User} from "../model/User";

module.exports = new EntitySchema({
    name: "User",
    target: User,
    columns: {
        id: {
            primary: true,
            type: "int",
            generationStrategy: "increment",
            isGenerated: true
        },
        facebookId: {
          type: "varchar"
        },
        email: {
            type: "varchar"
        },
        name: {
            type: "varchar"
        }
    }
});
