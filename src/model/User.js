/*export */ class User {
    constructor(id, facebookId, name, email) {
        this.id = id;
        this.facebookId = facebookId;
        this.name = name;
        this.email = email;
    }
}

module.exports = {
    User: User
};

