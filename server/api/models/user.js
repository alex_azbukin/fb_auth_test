import { getConnection } from "typeorm";
const User = require("../../../src/model/User").User;

let userModel = {};

userModel.findOrCreate = function (userParams = null) {
  if (userParams === null) {
    return {message: "Undefined user params", type: "error"};
  }
  let fbId = userParams.user.id;
  return getConnection().getRepository(User).findOne({facebookId: fbId}).then((user) => {
    if (typeof user != "undefined") {
      return {user: user, isNew: false, message: `User ${user.name} already exists.`, type: "info"};
    }
    else {
      let newUser = new User();
      newUser.name = userParams.user.name;
      newUser.email = userParams.user.email;
      newUser.facebookId = userParams.user.id;
      return getConnection().getRepository(User).save(newUser).then((savedUser) => {
        return {user: savedUser, isNew: true, message: `User ${savedUser.name} was created.`, type: "info"};
      }, (err) => {
        return {message: err.message, type: "error"};
      });
    }
  }, (err) => {
    return {message: err.message, type: "error"};
  });
};


export default userModel;
