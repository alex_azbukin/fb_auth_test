import { Router } from 'express';

import users from './controllers/users';

const router = Router();

// Add USERS Routes
router.use('/users', users);

export default router;
