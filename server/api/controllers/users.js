import { Router } from 'express';
import  userModel  from "../models/user";
const router = Router();


/* POST create or fetch user. */
router.post('/save', function (req, res, next) {
  let userParams = req.body;
  let user = userModel.findOrCreate(userParams);
  user.then((response) => {
    res.json(response);
  }, (err) => {
    res.json(err);
  });

});

export default router;
