import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import { Nuxt, Builder } from 'nuxt';
import bodyParser from 'body-parser';
const typeorm = require('typeorm');

typeorm.createConnection({
  "type": process.env.DB_TYPE,
  "host": process.env.DB_HOST,
  "port": process.env.DB_PORT,
  "username": process.env.DB_USER,
  "password": process.env.DB_PASS,
  "database": process.env.DB_NAME,
  "synchronize": false,
  "logging": false,
  "entities": [
    require('../src/entity/UserSchema')
  ],
}).then((connection) => {
  console.log("Database connected.");
}, (err) => {
  console.log(err.message);
  return process.exit(22);
}).catch((reason) => {
  console.log("REASON : ");
  console.log(reason);
  return process.exit(33);
});



const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3000;

app.set('port', port);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

import api from './api';

// Import API Routes
app.use('/api', api);

// Import and Set Nuxt.js options
let config = require('../nuxt.config');
config.dev = !(process.env.NODE_ENV === 'production');

// Init Nuxt.js
const nuxt = new Nuxt(config);

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt);
  builder.build();
}

// Give nuxt middleware to express
app.use(nuxt.render);

// Listen the server
app.listen(port, host);
console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
