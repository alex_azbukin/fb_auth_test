# test-fb-auth

> Facebook auth for test task

#### Configuration

``` bash
ormconfig.json => database config for migrations [Need to create, ormconfig.sample as example ]

.env => environment variables [Need to create, .env.sample as example ]

nuxt.config.js => change Facebook Client ID for your application in auth strategies

Create database fb_auth_test in your local environment
```

### Build Setup

``` bash
# install dependencies
$ npm install

# install typeorm [ORM for MySQL]
$ npm install typeorm -g

# install npx for launch scripts in typescript format
$ npm install -g npx

# migration UP
$ npx ts-node ./node_modules/typeorm/cli.js migration:run

# migration DOWN
$ npx ts-node ./node_modules/typeorm/cli.js migration:revert

# serve with hot reload at localhost:3000
$ npm run dev
```
