module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'test-fb-auth',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Facebook auth for test task' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],

  plugins: [
    { src: '~/plugins/vue-notifications.js', ssr: false }
  ],

  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    strategies: {
      facebook: {
        client_id: '387017368818843',
        userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,name,picture,email,birthday&redirect=false',
        scope: ['public_profile', 'email', 'user_birthday']
      },
    }
  },
  //Global middleware
  router: {
    middleware: ['auth']
  },
  axios: {
    // proxyHeaders: false
  }
}

